package jp.alhinc.shimizu_risa.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args){

		//コマンドライン引数が0もしくは2以上のときのエラー
		if(args.length != 1){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		HashMap<String, String> nameMap = new HashMap<String, String>();
		HashMap<String, Long> salesMap = new HashMap<String, Long>();

		BufferedReader br = null;

		if(!inputFile(args[0], "branch.lst","支店","\\d{3}", nameMap, salesMap)){
			return;
		}

		//ファイル名リストの一覧持ってきて、8桁.rcdをArrayListに入れました
		File dir = new File(args[0]);
		File[] files = dir.listFiles();

		ArrayList<String> fileNameList = new ArrayList<String>();

		for(int i = 0; i < files.length; i++){
			File salesFileName = files[i];

			String fileName = salesFileName.getName();

			if(fileName.matches("^\\d{8}.rcd$") && files[i].isFile()){      //8桁.rcdのファイルを抽出
				fileNameList.add(fileName);
			}
		}

		//ファイル名から数字のみ取り出し数値に変換

		ArrayList <Integer> fileNumberList = new ArrayList<Integer>();

		for(int i = 0; i < fileNameList.size(); i++){

			String fileName = fileNameList.get(i);
			String number = fileName.substring(0,8);

			int numberFile = Integer.parseInt(number);

			fileNumberList.add(numberFile);

		}


		for(int i = 0; i < fileNumberList.size()-1; i++){
			if(fileNumberList.get(i + 1)-fileNumberList.get(i) != 1){
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}

		//各ファイルを読み込む

		for(int i = 0; i < fileNameList.size(); i++){

			try{

				//支店コードと金額を分けるArrayListを作る
				ArrayList<String> salesList = new ArrayList<String>();

				File file = new File(args[0], fileNameList.get(i)) ; //売上リスト読み込む
				FileReader fr = new FileReader(file);
				br = new BufferedReader(fr);

				String line;
				while((line = br.readLine()) != null){
					salesList.add(line);
				}
				//売上データ3行以上あるときのエラー

				if(salesList.size() != 2){
					System.out.println( fileNameList.get(i) + "のフォーマットが不正です");
					return;
				}

				//支店コードが一致しないエラー

				if(!nameMap.containsKey(salesList.get(0))){
					System.out.println(fileNameList.get(i) + "の支店コードが不正です");
					return;
				}
				String sale =salesList.get(1);

				if(!sale.matches("^[0-9]*$")){     //売上ファイルの2行目が数字でないならエラー
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				Long sales = Long.parseLong(salesList.get(1));  //salesListから金額を取り出し、文字列からlongに変換
				Long eachSales = salesMap.get(salesList.get(0)); //Mapから金額を取り出す
				Long salesSum = eachSales + sales;               //金額を合計する


//				合計金額10桁以上のエラー処理

				if(salesSum > 9999999999L){
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				salesMap.put(salesList.get(0), salesSum); //Mapに(支店コード,金額)をいれた。

			} catch(IOException e){
				System.out.println("予期せぬエラーが発生しました");
				return;

			} finally{
				if(br != null){
					try {
						br.close();

					} catch(IOException e){
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}
		if(!outputFile(args[0],"branch.out" , nameMap, salesMap)){
			return;
		}
	}

	public static boolean inputFile(String path, String fileName,String category,String codeConditions, HashMap<String, String> nameMap,HashMap<String, Long> salesMap){
		BufferedReader br = null;

		try{
			File file = new File(path, fileName); //支店定義リスト読み込む

			if(!file.exists()){
				System.out.println(category + "定義ファイルが存在しません");
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			while((line = br.readLine()) != null){

				String[] items = line.split(",",-1); //,で文字を分ける

				if(items.length != 2){  //,と改行のエラー
					System.out.println(category + "定義ファイルのフォーマットが不正です");
					return false;
				}

				if(!items[0].matches(codeConditions)){     //支店コード3桁の数字でないならエラー
					System.out.println(category + "定義ファイルのフォーマットが不正です");
					return false;
				}

				nameMap.put(items[0], items[1]);
				salesMap.put(items[0],0L); //(支店コード,金額0を入れた)

			}


		} catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return false;

		} finally{
			if(br != null){
				try {
					br.close();

				} catch(IOException e){
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}


	public static boolean outputFile(String path, String fileName, HashMap<String, String> nameMap,HashMap<String, Long> salesMap){

		BufferedWriter bw = null;

		try {

			File file = new File(path, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			//branchNameMapとsalesMapから"支店コード,支店名,支店の売上"
			for(Map.Entry<String, String> entry : nameMap.entrySet()){

				bw.write(entry.getKey() + "," + entry.getValue() + "," + salesMap.get(entry.getKey()));
				bw.newLine();
			}

		} catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return false;

		} finally{
			if(bw != null){
				try {
					bw.close();

				} catch(IOException e){
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

}
